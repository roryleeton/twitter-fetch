class TwitterApi
	def self.get_followers(u)
		followerNames = []
		followers = client.followers(u).take(20).each do |twitter_user|
			followerNames += [twitter_user.name]
		end

		return followerNames
	end


	def self.client
		@client ||= Twitter::REST::Client.new do |config|
			config.consumer_key        = ENV["TWITTER_CONSUMER_KEY"]
			config.consumer_secret     = ENV["TWITTER_CONSUMER_SECRET"]
			config.access_token        = ENV["TWITTER_ACCESS_TOKEN"]
			config.access_token_secret = ENV["TWITTER_ACCESS_TOKEN_SECRET"]
		end
	end
end