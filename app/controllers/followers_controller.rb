class FollowersController < ApplicationController
	def create
		@user = User.find(params[:user_id])
		@follower = @user.followers.create(follower_params)
			
		redirect_to user_path(@user)
	end

	private
		def follower_params
			params.require(:follower).permit(:name)
		end
end
