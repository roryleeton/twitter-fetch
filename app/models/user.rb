class User < ApplicationRecord
	has_many :followers
	validates :username, presence: true, length:{ minimum: 5}
end
